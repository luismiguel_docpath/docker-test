FROM alpine:latest

RUN echo 'root:toor' | chpasswd
RUN echo '#!/bin/sh' > /run.sh &&\
    echo >> /run.sh &&\
    echo 'while true; do' >> /run.sh &&\
    echo '  sleep 30' >> /run.sh &&\
    echo 'done' >> /run.sh
RUN chmod 755 /run.sh

ENTRYPOINT "/run.sh"

